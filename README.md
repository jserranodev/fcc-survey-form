# fcc-survey-form

- FreeCodeCamp Responsive Web Design Certification. Project #2: Survey form.
- [Project to be solved.](https://www.freecodecamp.org/learn/responsive-web-design/responsive-web-design-projects/build-a-survey-form)
- [Solution to project.](https://jserranodev.gitlab.io/fcc-survey-form)
